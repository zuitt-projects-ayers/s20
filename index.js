/*
	JSON Objects
		- JSON stands for JavaScript Object Notation.
		- JSON can also be used in other programming languages.
		- Do not confuse JavaScript objects with JSON objects.
		- JSON is used for serializing different data types into bytes.
			- serialization - the process of converting data types into a series of bytes for easier transmission/transfer of information.
			- bytes - information that a computer processes to perform different tasks.

		- JSON objects use double quotes for property names.
		- Syntax:
			{
				"propertyA" : "valueA",
				"propertyB" " "valueB"
			};

			{
				"name" : "Yoon",
				"birthday" : "March 9, 1993"
			};

*/

// JSON Objects

	// {
	// 	"city" : "Quezon City",
	// 	"province" : "Metro Manila",
	// 	"country" : "Philippines"
	// }

// JSON Arrays

	// "cities" : [
	// 	{
	// 		"city" : "Quezon City",
	// 		"province" : "Metro Manila",
	// 		"country" : "Philippines"
	// 	},
	// 	{
	// 		"city" : "Cebu City",
	// 		"province" : "Cebu",
	// 		"country" : "Philippines"
	// 	}
	// ];

// JSON Methods
	// - JSON objects contain methods for parsing and converting data into stringified JSON.

	let batchesArr = [
		{batchName : "Batch 169"},
		{batchName : "Batch 170"}
	]

	console.log(JSON.stringify(batchesArr));

	let data = JSON.stringify({
		name : "Luke",
		age : 45,
		address : {
			city : "Manila",
			country : "Philippines"
		}
	});

	console.log(data)

// Using Stringify Method with Variables

	// User Details

	let firstName = prompt("First Name:");
	let lastName = prompt("Last Name:");
	let age = prompt("Age:");
	let address = {
		city : prompt("City:"),
		country : prompt("Country:")
	};

	let data2 = JSON.stringify({
		firstName : firstName,
		lastName : lastName,
		age : age,
		address : address
	});

	console.log(data2);

// Converting Stringified JSON into JavaScript Objects
	// (The JSON must first be a string (use `` or "").)

	let batchesJSON = `[
		{
			"batchName" : "Batch 169",
			"batch" : "five"
		},
		{
			"batchName" : "Batch 170"
		}
	]`

	console.log(JSON.parse(batchesJSON));